import { extendObservable } from 'mobx'

class Main {
  constructor() {
    extendObservable(this, {
      workplaces: []
    })
  }
}

export default new Main()
