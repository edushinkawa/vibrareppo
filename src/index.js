import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import Main from './Stores/Main'

import { Provider } from 'mobx-react'

const stores = { Main }

ReactDOM.render(
  <Provider {...stores}>
    <App />
  </Provider>,
  document.getElementById('root')
)
