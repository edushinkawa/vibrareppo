import React, { Component } from 'react'

import { Router, Route, browserHistory } from 'react-router'
import { observer, inject } from 'mobx-react'

import './App.css'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'

import LoginBox from './Components/LoginBox'
import Register from './Components/Register'
import Workplaces from './Components/Workplaces'
import Addworkplace from './Components/Addworkplace'

class App extends Component {
  render() {
    return (
      <section className="App">
        <Router history={browserHistory}>
          <Route path="/" component={LoginBox} />
          <Route path="/login" component={LoginBox} />
          <Route path="/register" component={Register} />
          <Route path="/workplaces" component={Workplaces} />
          <Route path="/addWorkplace" component={Addworkplace} />
        </Router>
      </section>
    )
  }
}

export default inject('Main')(observer(App))
