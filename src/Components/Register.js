import React, { Component } from 'react'

/* COMPONENTS */
import { observer, inject } from 'mobx-react'
import fire from '../Scripts/firebase'

class Register extends Component {
  constructor(props) {
    super(props)
    this.state = { email: null, password: null, registerSuccess: false }
  }

  onChange = event => {
    let obj = {}
    obj[event.target.name] = event.target.value
    this.setState(obj)
  }

  goToRoute = () => {
    this.props.router.push('/')
  }

  register = () => {
    fire
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        this.setState({
          registerSuccess: true
        })
      })
      .catch(error => {
        // Handle Errors here.
        var errorCode = error.code
        var errorMessage = error.message
        // ...
      })
  }

  const

  render() {
    const { email, password, registerSuccess } = this.state

    const SucessBox = () => (
      <div>
        <h1>Cadastro realizado com sucesso</h1>
        <h6 onClick={this.goToRoute}>Voltar para login</h6>
      </div>
    )

    return (
      <section className="LoginBoxContainer container align-middle">
        {!registerSuccess ? (
          <section className="LoginBox col-md-4">
            <h1>Cadastre-se</h1>
            <input
              type="email"
              placeholder="Email address"
              className="loginInput"
              name="email"
              onChange={this.onChange}
              required
              autofocus
            />
            <input
              type="password"
              placeholder="Password"
              name="password"
              className="loginInput"
              onChange={this.onChange}
              required
            />
            <button className="loginButton" type="submit" onClick={this.register}>
              Cadastrar
            </button>
          </section>
        ) : (
          <SucessBox />
        )}
      </section>
    )
  }
}

export default inject('Main')(observer(Register))
