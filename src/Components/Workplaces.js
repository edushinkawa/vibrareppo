import React, { Component } from 'react'

/* COMPONENTS */
import { observer, inject } from 'mobx-react'
import fire from '../Scripts/firebase'
import StarRatingComponent from 'react-star-rating-component'

class Workplaces extends Component {
  componentWillMount() {
    const { workplaces, count } = this.props.Main
    let storageRef = fire.database().ref()
    storageRef.once('value').then(snap => {
      let workplaces = snap.val().workplace
      let workplacesArr = []
      Object.keys(workplaces).map(workplace => {
        workplacesArr.push(workplaces[workplace])
      })
      this.props.Main.workplaces = workplacesArr
    })
  }

  goToRoute = () => {
    this.props.router.push('/addWorkplace')
  }

  render() {
    const { workplaces, count } = this.props.Main

    const WorkplacesCard = props => (
      <div>
        <h3>{props.name}</h3>
        <h5>Internet? {props.internet.hasInternet ? 'Sim' : 'Não possui'}</h5>
        <h5>
          Conforto:
          <StarRatingComponent
            name="rate1"
            starCount={5}
            value={props.comfort.rating * props.comfort.count / props.comfort.count}
          />
        </h5>
        <h5>
          Nível de barulho:
          <StarRatingComponent
            name="rate1"
            starCount={5}
            value={props.noise.rating * props.noise.count / props.noise.count}
          />
        </h5>
        <h5>
          Preço:
          <StarRatingComponent
            name="rate1"
            starCount={5}
            value={props.price.rating * props.price.count / props.price.count}
            renderStarIcon={() => <span> $</span>}
          />
        </h5>
        <hr style={{ borderTop: '1px solid rgba(226, 207, 207, 0.6)' }} />
      </div>
    )

    return (
      <section className="workplacesContainer">
        <section className="workplace container">
          <h1 className="text-center">Nestfinder<h6 onClick={this.goToRoute}>Adicionar nest</h6></h1>
          {workplaces.map(item => <WorkplacesCard {...item} />)}
        </section>
      </section>
    )
  }
}

export default inject('Main')(observer(Workplaces))
