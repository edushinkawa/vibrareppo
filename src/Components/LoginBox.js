import React, { Component } from 'react'

/* COMPONENTS */
import { observer, inject } from 'mobx-react'
import fire from '../Scripts/firebase'

class LoginBox extends Component {
  constructor(props) {
    super(props)
    this.state = { email: null, password: null, registerSuccess: false }
  }

  onChange = event => {
    let obj = {}
    obj[event.target.name] = event.target.value
    this.setState(obj)
  }

  login = () => {
    fire
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(() => {
        this.props.router.push('/workplaces')
      })
  }

  goToRoute = () => {
    this.props.router.push('/register')
  }

  render() {
    return (
      <section className="LoginBoxContainer container align-middle">
        <section className="LoginBox col-md-4">
          <h1>Bem-vindo ao Worknest!</h1>
          <input
            type="email"
            placeholder="Email address"
            className="loginInput"
            name="email"
            onChange={this.onChange}
            required
            autoFocus
          />
          <input
            type="password"
            placeholder="Password"
            className="loginInput"
            required
            name="password"
            onChange={this.onChange}
          />
          <button className="loginButton" type="submit" onClick={this.login}>
            Entrar
          </button>
          <h6 onClick={this.goToRoute}>Não tenho uma conta</h6>
        </section>
      </section>
    )
  }
}

export default inject('Main')(observer(LoginBox))
