import React, { Component } from 'react'

/* COMPONENTS */
import { observer, inject } from 'mobx-react'
import fire from '../Scripts/firebase'
import StarRatingComponent from 'react-star-rating-component'
import { storage } from 'firebase'

class Workplaces extends Component {
  constructor(props) {
    super(props)
    this.state = { name: null, comfort: 5, price: 5, noise: 5, checkbox: false }
  }

  goToRoute = () => {
    this.props.router.push('/workplaces')
  }

  onChange = event => {
    let obj = {}
    obj[event.target.name] = event.target.value
    this.setState(obj)
  }

  onChangeCheckbox = event => {
    let obj = {}
    obj[event.target.name] = event.target.value
    this.setState(obj)
  }

  onStarClickPrice(nextValue, prevValue, name) {
    this.setState({ price: nextValue })
  }

  onStarClickComfort(nextValue, prevValue, name) {
    this.setState({ comfort: nextValue })
  }

  onStarClickNoise(nextValue, prevValue, name) {
    this.setState({ noise: nextValue })
  }

  sendToFirebaseDatabse = () => {
    const { price, comfort, checkbox, name, noise, password } = this.state
    const postData = {
      price: { rating: price, count: 1 },
      comfort: { rating: comfort, count: 1 },
      internet: {
        hasInternet: checkbox,
        password: password
      },
      name: name,
      noise: { rating: noise, count: 1 }
    }
    let storageRef = fire.database().ref()

    let newPostKey = storageRef.child('/workplace').push().key
    let updates = {}
    updates['/workplace/' + newPostKey] = postData
    return fire
      .database()
      .ref()
      .update(updates)
      .then(() => {
        this.goToRoute()
      })
  }

  render() {
    const { price, comfort, checkbox, name, noise } = this.state

    return (
      <section className="workplacesContainer">
        <section className="workplace container text-center row">
          <section className="offset-md-3 col-md-6">
            <h1 className="text-center">Adicionar Nest</h1>
            <section className="LoginBox">
              <input
                type="text"
                placeholder="Nome do Local"
                className="loginInput"
                name="name"
                onChange={this.onChange}
              />
            </section>

            <section>
              <ul class="unstyled centered">
                <li>
                  <input
                    className="styled-checkbox"
                    id="styled-checkbox-1"
                    type="checkbox"
                    value={checkbox}
                    name="checkbox"
                    onChange={this.onChangeCheckbox}
                  />
                  <label for="styled-checkbox-1">Tem Wifi?</label>
                </li>
              </ul>
            </section>
            {this.state.checkbox && (
              <section className="LoginBox">
                <input
                  type="text"
                  placeholder="Senha do Wifi"
                  className="loginInput"
                  name="password"
                  onChange={this.onChange}
                />
              </section>
            )}
            <h5>
              Conforto:
              <StarRatingComponent
                name="comfort"
                starCount={5}
                value={comfort}
                onStarClick={this.onStarClickComfort.bind(this)}
              />
            </h5>
            <h5>
              Nível de barulho:
              <StarRatingComponent
                name="noise"
                starCount={5}
                value={noise}
                onStarClick={this.onStarClickNoise.bind(this)}
              />
            </h5>
            <h5>
              Preço:
              <StarRatingComponent
                name="price"
                starCount={5}
                value={price}
                renderStarIcon={() => <span> $</span>}
                onStarClick={this.onStarClickPrice.bind(this)}
              />
            </h5>
            <hr style={{ borderTop: '1px solid rgba(226, 207, 207, 0.6)' }} />

            <button className="loginButton" type="submit" onClick={this.sendToFirebaseDatabse}>
              Cadastrar
            </button>
          </section>
        </section>
      </section>
    )
  }
}

export default inject('Main')(observer(Workplaces))
